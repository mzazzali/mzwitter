package org.zee3;

import org.zee3.processor.AbstractProcessor;

import java.io.Console;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class CommandLineController {
    public CommandProcessorFactory factory = new CommandProcessorFactory();

    public void run(){
        Console console = System.console();
        if (console == null)
        {
            System.err.println("No console.");
            System.exit(1);
        }
        
        boolean keepRunning = true;
        while (keepRunning) {
            String command =  console.readLine(">> ");

            if ("exit".equals(command)){
                keepRunning = false;

            }else{
                AbstractProcessor processor = factory.createProcessor(command);
                processor.execute();

            }
        }
    }

}
