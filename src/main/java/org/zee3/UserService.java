package org.zee3;

import org.zee3.dao.Post;
import org.zee3.dao.User;

import java.util.*;

/**
 * Created by mzazzali on 3/23/2014.
 */
public class UserService {
    public Map<String, User> userStore = new HashMap<String, User>();
    // private Map<String, List<Post>> postHistoryStore = new HashMap<String, List<Post>>();

    private User getUser(String name) {
        User user = userStore.get(name);

        if (user == null) {
            user = createUser(name);
        }

        return user;
    }

    public User createUser(String name) {
        final User user = new User(name);
        userStore.put(name, user);
        return user;
    }

    public List<Post> getUserPostHistory(final String name) {
        final User user = getUser(name);
        final List<Post> posts = user.getPosts();

        return posts.subList(0, posts.size());
    }

    public void addUserPost(String name, String message) {
        final User user = getUser(name);

        final Post post = new Post(user, message);
        user.addPost(post);
    }

    public Set<User> getUserFollowees(String name) {
        final User user = getUser(name);

        return user.getFollows();
    }

    public void followUser(String follower, String followed) {
        final User followerUser = getUser(follower);
        final User followedUser = getUser(followed);

        followerUser.addFollows(followedUser);
    }

    public List<Post> getUserWall(String name) {
        final User user = getUser(name);
        final List<Post> wallPosts = new ArrayList<Post>();

        wallPosts.addAll(user.getPosts());

        for (User followedUser : user.getFollows()) {
            wallPosts.addAll(followedUser.getPosts());
        }

        Collections.sort(wallPosts, new Comparator<Post>() {
            @Override
            public int compare(Post post01, Post post02) {

                final Long timestamp01 = post01.getTimestamp();
                final Long timestamp02 = post02.getTimestamp();

                return timestamp02.compareTo(timestamp01);
            }
        });

        return wallPosts;
    }


    public List<Post> getUserPosts(String username) {
        final User user = getUser(username);
        return user.getPosts();
    }

}
