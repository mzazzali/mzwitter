package org.zee3.dao;

/**
 * Created by mzazzali on 3/23/2014.
 */
public class Post {
    private User user;
    private String message;
    private Long timestamp;

    public Post(final User user, final String message) {
        this.user = user;
        this.message = message;
        timestamp = System.nanoTime();
    }

    public User getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}
