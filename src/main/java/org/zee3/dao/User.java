package org.zee3.dao;

import java.util.*;

/**
 * Created by mzazzali on 3/23/2014.
 */
public class User {
   private String name;
    private List<Post> posts = new ArrayList<Post>();
    private Set<User> follows = new LinkedHashSet<User>();

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Post> getPosts() {
        return posts.subList(0, posts.size());
    }

    public void addPost(Post post) {
        posts.add(post);
    }

    public Set<User> getFollows() {
        return new LinkedHashSet<User>(follows);
    }

    public void addFollows(User user) {
        follows.add(user);
    }
}
