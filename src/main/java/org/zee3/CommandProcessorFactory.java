package org.zee3;

import org.apache.commons.lang3.StringUtils;
import org.zee3.processor.*;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class CommandProcessorFactory {
    private UserService userService = new UserService();

    public AbstractProcessor createProcessor(String command){
        final AbstractProcessor processor;

        final String[] parts = StringUtils.split(command, " ", 3);

        final String username = parts[0];

        if(parts.length==1){
            processor = new ReadUserPostsProcessor(username);

        }else if(parts.length==2){
            processor = new WallProcessor(username);

        }else if(StringUtils.equals(parts[1],"follows")){
            final String followedUser = parts[2];
            processor =  new FollowProcessor(username, followedUser);

        }else if(StringUtils.equals(parts[1], "->")) {
            final String message = parts[2];
            processor = new CreatePostProcessor(username, message);

        }else{
            processor = new UnknownCommandProcessor();
        }

        processor.setUserService(userService);

        return processor;
    }
}
