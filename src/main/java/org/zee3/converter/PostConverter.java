package org.zee3.converter;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class PostConverter {
    protected long getSecondsPassed(Long timestamp){
        final long minutes =  timestamp / 1000000000L;
        final long currentMinute = System.nanoTime() / 1000000000L;
        final long  secondsPassed = currentMinute - minutes;

        return Math.round(secondsPassed);
    }

    protected long convertToMinutesIfAvailable(long secondsPassed){
        if(secondsPassed > 59){
            return secondsPassed/60;
        }else{
            return secondsPassed;
        }
    }

    protected String getCorrectPlurality(long secondsPassed) {
        if(secondsPassed == 1){
            return "second";

        }else if(secondsPassed < 60) {
            return "seconds";

        }else if(secondsPassed < 120){
            return "minute";

        }else{
            return "minutes";
        }
    }
}
