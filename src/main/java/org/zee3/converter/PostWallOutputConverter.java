package org.zee3.converter;

import org.zee3.dao.Post;
import org.zee3.dao.User;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class PostWallOutputConverter extends PostConverter {

    public String convert(Post post){
        final long  secondsPassed = getSecondsPassed(post.getTimestamp());
        final User user = post.getUser();
        final String username = user.getName();
        final String output = username +
                " - " +
                post.getMessage() +
                " (" +
                convertToMinutesIfAvailable(secondsPassed) +
                " " +
                getCorrectPlurality(secondsPassed) +
                " ago)";

        return output;
    }
}
