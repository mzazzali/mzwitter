package org.zee3.processor;

import org.zee3.converter.PostWallOutputConverter;
import org.zee3.dao.Post;

import java.util.List;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class WallProcessor extends AbstractProcessor{
    private PostWallOutputConverter converter = new PostWallOutputConverter();
    private String username;

    public WallProcessor(String user) {
        this.username = user;
    }

    @Override
    public void execute() {
        final List<Post> allPosts = userService.getUserWall(username);

        for(Post post:allPosts){
            final String output = converter.convert(post);
            System.out.println(output);
        }
    }
}
