package org.zee3.processor;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class CreatePostProcessor extends AbstractProcessor{
    private String username;
    private String message;

    public CreatePostProcessor(String username, String message) {
        this.username = username;
        this.message = message;
    }

    @Override
    public void execute() {
        userService.addUserPost(username, message);
    }
}
