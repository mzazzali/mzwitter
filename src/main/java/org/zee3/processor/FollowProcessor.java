package org.zee3.processor;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class FollowProcessor extends AbstractProcessor{
    private String user;
    private String followingUser;

    public FollowProcessor(String user, String followingUser) {
        this.user = user;
        this.followingUser = followingUser;
    }

    public void execute(){
        userService.followUser(user, followingUser);
    }
}
