package org.zee3.processor;

import org.zee3.UserService;

/**
 * Created by mzazzali on 3/26/2014.
 */
public abstract class AbstractProcessor {
    protected UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public abstract void execute();
}
