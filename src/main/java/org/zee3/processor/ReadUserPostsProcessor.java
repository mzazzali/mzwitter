package org.zee3.processor;

import org.zee3.converter.PostSingleUserOutputConverter;
import org.zee3.dao.Post;
import org.zee3.dao.User;

import java.util.List;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class ReadUserPostsProcessor extends AbstractProcessor{
    private PostSingleUserOutputConverter converter = new PostSingleUserOutputConverter();
    private String username;

    public ReadUserPostsProcessor(String username) {
        this.username = username;
    }

    @Override
    public void execute() {
        final List<Post> posts = userService.getUserPosts(username);

        for(Post post:posts){
            final String output = converter.convert(post);
            System.out.println(output);
        }
    }
}
