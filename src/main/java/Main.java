import org.zee3.CommandLineController;

/**
 * Created by mzazzali on 3/23/2014.
 */
public class Main {

    public static void main(String [] args){
        final CommandLineController controller = new CommandLineController();

        controller.run();
    }

}
