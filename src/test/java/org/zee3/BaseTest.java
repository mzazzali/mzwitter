package org.zee3;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class BaseTest {
    protected static final String TEST_USER_1 = "Alice";
    protected static final String TEST_USER_2 = "Bob";
    protected static final String TEST_POST_1 = "My Test Post 1";
    protected static final String TEST_POST_2 = "My Test Post 2";
    protected static final String TEST_POST_3 = "My Test Post 3";
}
