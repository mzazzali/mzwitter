package org.zee3;

import org.junit.Before;
import org.junit.Test;
import org.zee3.CommandProcessorFactory;
import org.zee3.UserService;
import org.zee3.processor.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class CommandProcessorFactoryTest {
    private final static String WALL_COMMAND = "Bob wall";
    private final static String FOLLOW_COMMAND = "Bob follows Alice";
    private final static String READ_COMMAND = "Bob";
    private final static String POST_COMMAND = "Bob -> MY TEST MESSAGE";

    private UserService userService;
    private CommandProcessorFactory commandProcessorFactory;

    @Before
    public void setUp() throws Exception {
        userService = spy(new UserService());
        commandProcessorFactory = new CommandProcessorFactory();
    }

    @Test
    public void createProcessorTestForWallProcessor(){
        AbstractProcessor processor = commandProcessorFactory.createProcessor(WALL_COMMAND);

        assertEquals(WallProcessor.class.getCanonicalName(), processor.getClass().getCanonicalName());
    }

    @Test
    public void createProcessorTestForFollowsProcessor(){
        AbstractProcessor processor = commandProcessorFactory.createProcessor(FOLLOW_COMMAND);

        assertEquals(FollowProcessor.class.getCanonicalName(), processor.getClass().getCanonicalName());
    }

    @Test
    public void createProcessorTestForPostProcessor(){
        AbstractProcessor processor = commandProcessorFactory.createProcessor(POST_COMMAND);

        assertEquals(CreatePostProcessor.class.getCanonicalName(), processor.getClass().getCanonicalName());
    }

    @Test
    public void createProcessorTestForReadProcessor(){
        AbstractProcessor processor = commandProcessorFactory.createProcessor(READ_COMMAND);

        assertEquals(ReadUserPostsProcessor.class.getCanonicalName(), processor.getClass().getCanonicalName());
    }

    @Test
    public void createProcessorTestForUnknownCommandProcessor(){
        AbstractProcessor processor = commandProcessorFactory.createProcessor("UNKNOWN");

        assertEquals(ReadUserPostsProcessor.class.getCanonicalName(), processor.getClass().getCanonicalName());
    }
}
