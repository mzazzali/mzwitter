package org.zee3;

import org.zee3.dao.Post;
import org.zee3.dao.User;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * Created by mzazzali on 3/23/2014.
 */

public class UserServiceTest extends BaseTest{

    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userService = new UserService();
    }

    @Test
    public void createUserTest() {
        final User user = userService.createUser(TEST_USER_1);

        assertEquals(TEST_USER_1, user.getName());
    }

    @Test
    public void getUserPostHistoryTest() {
        final List<Post> posts = userService.getUserPostHistory(TEST_USER_1);

        assertEquals(0, posts.size());
    }

    @Test
    public void addUserPostHistoryTest() {
        userService.addUserPost(TEST_USER_1, TEST_POST_1);

        final List<Post> posts = userService.getUserPostHistory(TEST_USER_1);
        assertEquals(TEST_POST_1, posts.get(0).getMessage());
    }

    @Test
    public void addUserFollowsTest() {
        userService.addUserPost(TEST_USER_1, TEST_POST_1);
        userService.followUser(TEST_USER_2, TEST_USER_1);
        final Set<User> followees = userService.getUserFollowees(TEST_USER_2);
        final Iterator<User> followeesIterator = followees.iterator();
        final User firstUser = followeesIterator.next();
        final List<Post> posts = firstUser.getPosts();
        final Post firstPost = posts.get(0);

        assertEquals(TEST_POST_1, firstPost.getMessage());
    }

    @Test
    public void addUserFollowsTestFollowUserTwice() {
        userService.addUserPost(TEST_USER_1, TEST_POST_1);
        userService.followUser(TEST_USER_2, TEST_USER_1);
        userService.followUser(TEST_USER_2, TEST_USER_1);
        final Set<User> followees = userService.getUserFollowees(TEST_USER_2);

        assertEquals(1, followees.size());
    }

    @Test
    public void getUserWallTest(){
        userService.addUserPost(TEST_USER_1, TEST_POST_1);
        userService.addUserPost(TEST_USER_2, TEST_POST_2);
        userService.addUserPost(TEST_USER_1, TEST_POST_3);

        userService.followUser(TEST_USER_1, TEST_USER_2);

        final List<Post> posts = userService.getUserWall(TEST_USER_1);

        assertEquals(TEST_POST_3, posts.get(0).getMessage());
        assertEquals(TEST_POST_2, posts.get(1).getMessage());
        assertEquals(TEST_POST_1, posts.get(2).getMessage());

    }
}
