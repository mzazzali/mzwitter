package org.zee3.processor;

import org.mockito.Mockito;
import org.zee3.BaseTest;
import org.zee3.UserService;
import org.zee3.dao.User;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class FollowProcessorTest extends BaseTest {

    private FollowProcessor processor;
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userService = spy(new UserService());
        processor = new FollowProcessor(TEST_USER_1, TEST_USER_2);
        processor.setUserService(userService);
    }

    @Test
    public void executeTest(){

        processor.execute();

        verify(userService, Mockito.atLeastOnce()).followUser(anyString(), anyString());
    }
}
