package org.zee3.processor;

import org.zee3.BaseTest;
import org.zee3.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class WallProcessorTest extends BaseTest {
    private WallProcessor processor;
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userService = spy(new UserService());
        processor = new WallProcessor(TEST_USER_1);
        processor.setUserService(userService);
    }

    @Test
    public void executeTest(){

        userService.addUserPost(TEST_USER_1, TEST_POST_1);
        userService.addUserPost(TEST_USER_2, TEST_POST_2);
        userService.addUserPost(TEST_USER_1, TEST_POST_3);

        userService.followUser(TEST_USER_1, TEST_USER_2);

        processor.execute();

        verify(userService, Mockito.times(1)).getUserWall(anyString());

    }
}
