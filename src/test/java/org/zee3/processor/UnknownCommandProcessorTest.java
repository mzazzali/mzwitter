package org.zee3.processor;

import org.zee3.UserService;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class UnknownCommandProcessorTest {
    private UnknownCommandProcessor processor;
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userService = spy(new UserService());
        processor = new UnknownCommandProcessor();
        processor.setUserService(userService);
    }

    @Test
    public void executeTest(){

        processor.execute();

        verifyZeroInteractions(userService);

    }
}
