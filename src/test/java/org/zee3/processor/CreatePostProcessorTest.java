package org.zee3.processor;

import org.zee3.BaseTest;
import org.zee3.UserService;
import org.zee3.dao.Post;
import org.zee3.dao.User;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class CreatePostProcessorTest extends BaseTest {
    private CreatePostProcessor processor;
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userService = new UserService();
        processor = new CreatePostProcessor(TEST_USER_1, TEST_POST_1);
        processor.setUserService(userService);
    }

    @Test
    public void executeTest(){

        processor.execute();

        final List<Post> posts = userService.getUserPosts(TEST_USER_1);

        assertEquals(TEST_POST_1, posts.get(0).getMessage());
    }
}
