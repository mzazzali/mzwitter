package org.zee3.converter;

import org.zee3.BaseTest;
import org.zee3.dao.Post;
import org.zee3.dao.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Created by mzazzali on 3/26/2014.
 */
public class PostSingleUserOutputConverterTest extends BaseTest {
    private PostSingleUserOutputConverter postSingleUserOutputConverter;

    @Before
    public void setUp() throws Exception {

        postSingleUserOutputConverter = new PostSingleUserOutputConverter();
    }

    @Test
    public void executeTestWith1Minute() throws InterruptedException {
        final String expectedMessage = TEST_POST_1 + " (1 minute ago)";
        final User user = new User(TEST_USER_1);
        final Post post = spy(new Post(user, TEST_POST_1));

        when(post.getTimestamp()).thenReturn(System.nanoTime()-60000000000L);

        final String output = postSingleUserOutputConverter.convert(post);

        assertEquals(expectedMessage , output);

    }


    @Test
    public void executeTestWith2Minutes() throws InterruptedException {
        final String expectedMessage = TEST_POST_1 + " (2 minutes ago)";
        final User user = new User(TEST_USER_1);
        final Post post = spy(new Post(user, TEST_POST_1));

        when(post.getTimestamp()).thenReturn(System.nanoTime()-120000000000L);

        final String output = postSingleUserOutputConverter.convert(post);

        assertEquals(expectedMessage , output);

    }

    @Test
    public void executeTestWith1Second() throws InterruptedException {
        final String expectedMessage = TEST_POST_1 + " (1 second ago)";
        final User user = new User(TEST_USER_1);
        final Post post = spy(new Post(user, TEST_POST_1));

        when(post.getTimestamp()).thenReturn(System.nanoTime()-1000000000L);

        final String output = postSingleUserOutputConverter.convert(post);

        assertEquals(expectedMessage , output);

    }

    @Test
    public void executeTestWith3Seconds() throws InterruptedException {
        final String expectedMessage = TEST_POST_1 + " (3 seconds ago)";
        final User user = new User(TEST_USER_1);
        final Post post = spy(new Post(user, TEST_POST_1));

        when(post.getTimestamp()).thenReturn(System.nanoTime()-3000000000L);

        final String output = postSingleUserOutputConverter.convert(post);

        assertEquals(expectedMessage , output);

    }
}
